/**
 * Regen system:
 * - 20 hunger: 1 health / 2 seconds
 * - >=18 hunger: 1 health / 4 seconds
 */
package com.onions;

import java.util.HashSet;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.event.entity.*;
import org.bukkit.entity.*;
import org.bukkit.event.player.*;

public class Oldregen extends JavaPlugin implements Listener {
	public static JavaPlugin plugin;
	HashSet<Player> regenerating = new HashSet<>();
	
	private static Long REGEN_PERIOD = 40L;
	private static int MIN_HUNGER_TO_REGEN = 18;

	@Override
	public void onEnable() {
		Oldregen.plugin = this;
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		regenerating.clear();

		// runs periodic check to apply regen on players
		Bukkit.getScheduler().runTaskTimer(this, () -> {
			for ( Player p: Bukkit.getServer().getOnlinePlayers() ) {
				if ( p.getHealth() < 20 && p.getFoodLevel() >= MIN_HUNGER_TO_REGEN ) {
					if ( !regenerating.contains(p) ) {
						regenerating.add(p);
						BukkitTask task = new HealthRegenTask(p).runTaskTimer(this, 0, REGEN_PERIOD);
					}
				}
			}
		}, 1, 10);
	}
	
	// add some delay between regen task and event
	@EventHandler
	public void onDamageEvent(EntityDamageEvent event) {
		if ( event.getEntityType().equals(EntityType.PLAYER) ) {
			Player p = (Player) event.getEntity();
			if ( p.getHealth() < 20 && p.getFoodLevel() >= MIN_HUNGER_TO_REGEN ) {
				if ( !regenerating.contains(p) ) {
					regenerating.add(p);
					BukkitTask task = new HealthRegenTask(p).runTaskTimer(this, 20, REGEN_PERIOD);
				}
			}
		}
	}
	
	// add some delay between regen task and event
	@EventHandler
	public void onEatingEvent(PlayerItemConsumeEvent event) {
		Player p = event.getPlayer();
		if ( p.getHealth() < 20 && p.getFoodLevel() >= MIN_HUNGER_TO_REGEN ) {
			if ( !regenerating.contains(p) ) {
				regenerating.add(p);
				BukkitTask task = new HealthRegenTask(p).runTaskTimer(this, 20, REGEN_PERIOD);
			}
		}
	}
	
	@EventHandler
	public void onLoginEvent(PlayerLoginEvent event) {
		Player p = event.getPlayer();
		if ( p.getHealth() < 20 && p.getFoodLevel() >= MIN_HUNGER_TO_REGEN ) {
			if ( !regenerating.contains(p)) {
				regenerating.add(p);
				BukkitTask task = new HealthRegenTask(p).runTaskTimer(this, 0, REGEN_PERIOD);
			}
		}
	}
	
	@EventHandler
	public void onLogoutEvent(PlayerQuitEvent event) {
		regenerating.remove(event.getPlayer());
	}
	
	/**
	 * Does regen
	 */
	private class HealthRegenTask extends BukkitRunnable {
		
		private final Player player;
		private int tick = 1;

		public HealthRegenTask(Player player) {
			this.player = player;
		}
		
		@Override
		public void run() {
			try {
				if ( !player.isDead() && player.isOnline() ) {
					if ( player.getHealth() < 20.0 ) {
						if ( player.getFoodLevel() == 20 ) {
							player.setHealth(player.getHealth() + 1);
						}
						else if ( player.getFoodLevel() >= MIN_HUNGER_TO_REGEN ) {
							if ( this.tick <= 0 ) {
								player.setHealth(player.getHealth() + 1);
								this.tick = 1;
							} else {
								this.tick -= 1;
							}
						}
						else {
							regenerating.remove(player);
							this.cancel();
						}
					}
					else {
						regenerating.remove(player);
						this.cancel();
					}
				}
				else {
					regenerating.remove(player);
					this.cancel();
				}
			}
			catch (Exception IllegalArgumentException) {
				regenerating.remove(player);
				this.cancel();
			}
		}
	}
}